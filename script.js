class Employee {
    constructor(name, age, salary){
        this.name = name    
        this.age = age
        this.salary = salary
    }
   set name(name){
    this._name = name
   }
    set age(age){
        this._age = age
    }
   set salary(salary){
    this._salary = salary
   }
   get name(){
    return this._name 
   }
   get age(){
    return this._age
   }
   get salary(){
    return this._salary
   }
}
 let user = new Employee("Eugene", 35, 20000);

class Programmer extends Employee {
    constructor(name, age, salary,lang) {
        super(name, age, salary);
        this.lang = lang
    }
    set lang(lang){
        this._lang = lang
    }
    get lang() {
        return this._lang
    }
    getSalary(){
        return this._salary * 3
    }
}
let programmers = [
    new Programmer("Eugene", 35, 20000, 3),
    new Programmer("Kate", 25, 10000, 2),
    new Programmer("Alex", 45, 250000, 4),
    new Programmer("Max", 35, 20000, 3),
    new Programmer("Andrey", 27, 8000, 2),
    new Programmer("Anton", 65, 750000, 4),
    new Programmer("Misha", 33, 160700, 3),
    new Programmer("Ulya", 32, 56700, 2),
    new Programmer("Alex", 23, 7000, 4)
]
programmers.forEach((programmer)=>{
    console.log(programmer);
})
